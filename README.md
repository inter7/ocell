# Ocell

This project is an interview question proposed by Ocell

## Getting started

Problem:
1) Given a list of tuples T, where each tuple consists of a value x and y, and a bounding box B,
consisting of two tuples defining its lower left (x,y) and upper right(x,y) position,
define a function that returns all instances of T that are contained by B.
Describe a naive version of the function in pseudocode, or a high-level language of your choosing.

2) Now consider there are many bounding boxes. E.g. we have millions of tuples in T and thousands of bounding boxes b. How would you change/optimize the code to improve the run time?
