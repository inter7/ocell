class point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Point (X :{0},Y :{1})".format(self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return self.x != other.x or self.y != other.y

    def __hash__(self):
        return hash((self.x, self.y))


class bound:
    def __init__(self, min_point, max_point):
        self.min_point = min_point
        self.max_point = max_point

    def __lt__(self, other):
        return (self.max_point.x - self.min_point.x) + (self.max_point.y - self.min_point.Y) < (
                other.max_point.x - other.min_point.x) + (
                       other.max_point.y - other.min_point.Y)

    def __le__(self, other):
        return (self.max_point.x - self.min_point.x) + (self.max_point.y - self.min_point.Y) <= (
                other.max_point.x - other.min_point.x) + (
                       other.max_point.y - other.min_point.Y)

    def __eq__(self, other):
        return self.min_point == other.min_point and self.max_point == other.max_point

    def __ne__(self, other):
        return self.min_point != other.min_point or self.max_point != other.max_point

    def __gt__(self, other):
        return (self.max_point.x - self.min_point.x) + (self.max_point.y - self.min_point.Y) > (
                other.max_point.x - other.min_point.x) + (
                       other.max_point.y - other.min_point.Y)

    def __ge__(self, other):
        return (self.max_point.x - self.min_point.x) + (self.max_point.y - self.min_point.Y) >= (
                other.max_point.x - other.min_point.x) + (
                       other.max_point.y - other.min_point.Y)

    def __hash__(self):
        return hash((self.min_point, self.max_point))

    def __repr__(self):
        return "Bound (min_point :{0},max_point :{1})".format(self.min_point, self.max_point)


class min_fitting_bound_max_non_fitting_bound:
    def __init__(self, min_fitting_bound, max_non_fitting_bound):
        self.min_fitting_bound = min_fitting_bound
        self.max_non_fitting_bound = max_non_fitting_bound


class point_contained_not_contained:
    def __init__(self, point_contained, point_not_contained):
        self.point_contained = point_contained
        self.point_not_contained = point_not_contained


def point_is_in_bound(p, b):
    if b.min_point.x <= p.x <= b.max_point.x:
        return b.min_point.y <= p.y <= b.max_point.y
    return False


def bound1_in_bound2(bound1, bound2):
    if not bound1 or not bound2:
        return False
    if bound2.min_point.x > bound1.min_point.x:
        return False
    if bound2.min_point.y > bound1.min_point.y:
        return False
    if bound2.max_point.x < bound1.max_point.x:
        return False
    return bound2.max_point.y >= bound1.max_point.y


# Case we have thousands of tuples in T and millions of bounding boxes b
# In this case we have fixed million bounding boxes and each time we run it with tuples of points
# mem here has key: point and value min_fitting_bound_max_non_fitting_bound
def get_points_in_bound(T, B, mem={}):
    # declare vars
    returnRes = []
    b = bound(point(B[0][0], B[0][1]), point(B[1][0], B[1][1]))

    # check if points are in bound
    for t in T:
        p = point(t[0], t[1])
        # check if we can optimize by using cache
        if p in mem:
            cached_bound = mem[p]
            # if our Bound contains min_fitting_bound then we already contain the point
            if bound1_in_bound2(cached_bound.min_fitting_bound, b):
                returnRes.append(p)
                continue
            else:
                # if our Bound is inside max_non_fitting_bound then the point already don't fit
                if bound1_in_bound2(b, cached_bound.max_non_fitting_bound):
                    continue

        if point_is_in_bound(p, b):
            returnRes.append(p)
            if not p in mem:
                mem[p] = min_fitting_bound_max_non_fitting_bound(b, None)
            else:
                cached_bound = mem[p]
                cached_bound.min_fitting_bound = b
        else:
            if not p in mem:
                mem[p] = min_fitting_bound_max_non_fitting_bound(None, b)
            else:
                cached_bound = mem[p]
                cached_bound.max_non_fitting_bound = b
    return returnRes, mem


# Case we have millions of tuples in T and thousands of bounding boxes b
# In this case we have fixed million tuple and each time we run it with new B
# mem here has key: bound and value T list of points contained
def get_points_in_bound_with_millions_of_points(T, B, mem={}):
    # map fields
    if len(T) == 0:
        return []
    # convert my T to list point so I can do substruciton
    listPoints = []
    for t in T:
        p = point(t[0], t[1])
        listPoints.append(p)
    returnRes = []
    b = bound(point(B[0][0], B[0][1]), point(B[1][0], B[1][1]))
    if mem and b in mem:
        return mem[b]
    else:
        list_bounds_that_i_contain = []
        list_bounds_that_contains_me = []
        for v in mem:
            if bound1_in_bound2(v, b):
                list_bounds_that_i_contain.append(v)
            if bound1_in_bound2(b, v):
                list_bounds_that_contains_me.append(v)
        # if we contain many bounds we select biggest one and get the points it contains and don't compute them
        if len(list_bounds_that_i_contain) > 0:
            maxBoundIContain = max(list_bounds_that_i_contain)
            list_cached_points = mem[maxBoundIContain]
            listPoints = list(set(listPoints) - set(list_cached_points))
            returnRes = list_cached_points
        #  we check if we are inside some bound we remove the point it don't contain so we don't compute them
        if len(list_bounds_that_contains_me) > 0:
            minBoundContainsMe = min(list_bounds_that_i_contain)
            list_cached_points = mem[minBoundContainsMe]
            # we remove point that was outside of the bound that contains me
            listPointToRemove = list(set(T) - set(list_cached_points))
            listPoints = list(set(listPoints) - set(listPointToRemove))  # or just T = list_cached_points
        for p in listPoints:
            if point_is_in_bound(p, b):
                returnRes.append(p)
        mem[b] = returnRes
        # we can just reuse mem if this method accepts list of Bound, instead of returning mem and reusing it
        return returnRes, mem


if __name__ == '__main__':
    T = [(1, 2), (7, 8), (-1, 5), (-7, 3), (4, 9), (11, 15), (-5, 1), (-3, -2), (4, 3), (77, 77)]
    b = [(-3, -3), (4, 4)]
    returnPointsInBoundResponse = get_points_in_bound(T, b)
    result = returnPointsInBoundResponse[0]
    mem = returnPointsInBoundResponse[1]
    for r in result:
        print(r)
    print('now second run')
    b = [(-10, -10), (10, 10)]
    returnPointsInBoundResponse = get_points_in_bound(T, b, mem)
    result = returnPointsInBoundResponse[0]
    mem = returnPointsInBoundResponse[1]
    for r in result:
        print(r)
    print('now 3rd run')
    b = [(-2, -2), (2, 2)]
    returnPointsInBoundResponse = get_points_in_bound(T, b, mem)
    result = returnPointsInBoundResponse[0]
    mem = returnPointsInBoundResponse[1]
    for r in result:
        print(r)

    print('now using second method')
    returnPointsInBoundResponse = get_points_in_bound_with_millions_of_points(T, b)
    result = returnPointsInBoundResponse[0]
    mem = returnPointsInBoundResponse[1]
    for r in result:
        print(r)
    b = [(-3, -3), (4, 4)]
    returnPointsInBoundResponse = get_points_in_bound_with_millions_of_points(T, b, mem)
    result = returnPointsInBoundResponse[0]
    mem = returnPointsInBoundResponse[1]
    for r in result:
        print(r)
